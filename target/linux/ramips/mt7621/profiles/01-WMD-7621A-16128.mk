#
# Copyright (C) 2011 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#

define Profile/WMD-7621A-16128
	NAME:=WMD-7621A-16128
	PACKAGES:=\
		kmod-usb-core kmod-usb3 kmod-usb-ohci \
		kmod-ledtrig-usbdev \
                mountd \
                mjpg-streamer \
                uhttpd rpcd rpcd-mod-iwinfo \
                rpcd-mod-rpcsys cgi-io spi-tools \
                kmod-fs-vfat kmod-fs-exfat kmod-fs-ext4 \
        kmod-i2c-core kmod-i2c-mt7621 \
        kmod-nls-base kmod-nls-cp437 kmod-nls-iso8859-1 kmod-nls-utf8 \
        mtk-wifi luci \
        -swconfig -rt2x00 \
        ated hwnat reg gpio btnd switch ethstt uci2dat mii_mgr watchdog 8021xd eth_mac\
        wireless-tools block-mount fstools kmod-scsi-generic \
        kmod-usb-storage \
        kmod-fs-ntfs kmod-nls-cp936 \
        kmod-nls-cp850 kmod-nls-iso8859-1 kmod-nls-iso8859-15 kmod-nls-cp950
endef

define Profile/WMD-7621A-16128/Description
	Default package set compatible with most boards.
endef
$(eval $(call Profile,WMD-7621A-16128))
